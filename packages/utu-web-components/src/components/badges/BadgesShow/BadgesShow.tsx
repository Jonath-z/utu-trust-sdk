/* eslint-disable no-nested-ternary */
import {h} from "preact";
import {useState} from "preact/hooks";
import {omit} from "lodash";
import { BaseComponent } from "../../common/BaseComponent";
import {
  ATTR_THEME_COLOR
} from "../../../names";
import { IFeedbackDetailsProps } from "../../screens/FeedbackDetails/FeedbackDetailsProps";
import style from "./BadgesShow.scss";
import FeedbackDetailsShimmerAnimation from "../../common/SubmitStatusView/FeedbackDetailsShimmerAnimation";
import {NoBadgeLogo} from "../../../assets/Images"



export default function BadgesShow(props: IFeedbackDetailsProps) {

  // environment conditionals
  const [isDark, setIsDark] = useState(false);
  const envCondition =
      isDark ? "dark" : "light"
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }
  

  // fet badges and keys
  const summaryText = props.feedbackSummary?.badges?.summaryText;
  const fbBadges = props.feedbackSummary?.badges;
  const Badge = omit(fbBadges, "summaryText");
  const keys = Object.keys(Badge)

  // returns the badge with image, thumb and title
  const badgeGroup = keys.map((key, i) => {
    const badges = Badge[key];
    const badgeKeys = Object.keys(badges);
    if (!badgeKeys) console.log("no badge key, should error out")

    return badgeKeys.map((badgeKey: string) => {

      const qualifiersMain = badges[badgeKey].qualifiers.positive ? badges[badgeKey].qualifiers.positive :
        badges[badgeKey].qualifiers.negative? badges[badgeKey].qualifiers.negative : undefined;
      if(!qualifiersMain) return null
      return (<div key={badgeKey} className="badge-spacing">
        <div className="badge-container">
          <img
            className={`badge-container-thumb badge-container-thumb-${envCondition} badge-container-position-child-thumb`}
            src={qualifiersMain?.badge?.image}
            alt="person face"
            onError={e => {
              (e.target as HTMLImageElement).onerror = null;
              (e.target as HTMLImageElement).src = "";
              (e.target as HTMLImageElement).className = "badge-container-img badge-container-img-no-data";
              (e.target as HTMLImageElement).alt = "";
            }}/>
          <img className="badge-container-img" src={badges[badgeKey].image || ""} alt="person face"
            onError={e => {
              (e.target as HTMLImageElement).onerror = null;
              (e.target as HTMLImageElement).src = "";
              (e.target as HTMLImageElement).className = "badge-container-img badge-container-img-no-data";
              (e.target as HTMLImageElement).alt = "";
            }}/>
          <div className="badge-container-profile-container">
            {qualifiersMain?.summaryImages?.map(
              (img: string) => (
                <img
                  className={`badge-container-profile badge-container-profile-${envCondition} badge-container-position-child-profile`}
                  src={img}
                  alt="person face"
                  onError={e => {
                    (e.target as HTMLImageElement).onerror = null;
                    (e.target as HTMLImageElement).src = "";
                    (e.target as HTMLImageElement).className = "badge-container-img badge-container-img-no-data";
                    (e.target as HTMLImageElement).alt = "";
                  }} />))
            }
          </div>
          <div className={`badge-container-title badge-container-title-${envCondition}`}>{badgeKey}</div>
        </div>
      </div>)
    })
  })


  const badgeGroupNoData = (
    <div className="placeholder-card">
      <div className="placeholder-icon"><NoBadgeLogo {...props} /></div>
      <div className="placeholder-text-wrap">
        <div className="placeholder-text-title">No badges available</div>
        <div className="placeholder-text-body">
            When people in your network leave badges for this entity they will appear here.
        </div>
      </div>
    </div>
  )

  return (
    <FeedbackDetailsShimmerAnimation submitStatus={props.submitStatus}>

      <BaseComponent
        style={style}
        className="badge-section"
        excludeBootstrap
        excludeFonts
      >
        <p><b> Badges from people in your network</b> </p>
        {fbBadges?.assignments === undefined || Object.keys(fbBadges?.assignments).length === 0 ?

          <section className={`badge-section-content x-utu-section x-utu-section-${envCondition}`}>
            <div className="badge-section-badges-no-data">{badgeGroupNoData}</div>
          </section>
          :
          <section className="badge-section-content">
            <div className="badge-section-badges">{badgeGroup}</div>
            <div className="badge-section-text">
              <p className="badge-section-text-content">
                {summaryText}
              </p>
            </div>
          </section>
        }
      </BaseComponent>
    </FeedbackDetailsShimmerAnimation>
  );
}

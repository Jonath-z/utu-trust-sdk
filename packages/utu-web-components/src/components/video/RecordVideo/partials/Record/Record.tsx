/* eslint max-len: 0 */

import { h } from "preact";
import style from "./Record.scss";

export default function recordIcon() {

  return <svg style={style} version="1.1" className="x-utu-record-play-btn" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 120 120" enableBackground="new 0 0 120 120" xmlSpace="preserve">
    <circle cx="54" cy="60" r="30" className="x-utu-record-play"/>
  </svg>

}
/* eslint-disable react/no-unstable-nested-components */
import { h } from "preact";
import { useEffect, useLayoutEffect, useRef, useState } from "preact/hooks";
import "./Player.scss";
import Logo from "../../../../assets/Images";
import Play from "../icons/play";
import Pause from "../icons/pause";
import Expand from "../icons/expand";
import Mute from "../icons/mute";
import Volume from "../icons/volume";

export default function (
  videoUrl: string,
  preview?: boolean,
  previewStream?: MediaStream,
  showControls?: boolean
) {
  const [controlsVisible, setControlsVisible] = useState(false);
  const [isPlaying, setIsPlaying] = useState(true);
  const [isMuted, setIsMuted] = useState(false);


  const videoRef = useRef<HTMLVideoElement>(null);


  useLayoutEffect(() => {
    function updateSize() {
      // setSize({width: window.innerWidth, height: window.innerHeight});
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);


  const handlePlay = () => {
    if (isPlaying) {
      videoRef.current.pause();
      setIsPlaying(false);
    } else {
      videoRef.current.play();
      setIsPlaying(true);
    }
  };

  // const getDuration = ()=> {
  //   let dur = videoRef.current.duration;
  //   dur = Number(dur.toFixed());
  //   // const formatted = "";
  //   // setLength(dur);
  //   // setFormattedLength(formatted);

  //   return dur;
  // }


  const handleMute = () => {
    if (isMuted) {
      videoRef.current.muted = false;
      setIsMuted(false);
    } else {
      videoRef.current.muted = true;
      setIsMuted(true);
    }
  };


  const fullScreen = async () => {
    await videoRef.current.requestFullscreen();
  };


  useEffect(() => {
    if (videoRef.current) {
      setControlsVisible(true);
    }
  }, [videoUrl]);



  function Preview({ stream }: { stream: MediaStream | null }) {
    const previewRef = useRef<HTMLVideoElement>(null);

    useEffect(() => {
      if (previewRef.current && stream && preview) {
        previewRef.current.srcObject = stream;
      }
    }, [stream]);

    if (!stream) {
      return (
        <div className='trust-video-wrapper trust-video-msg'>
          <p>Kindly Press the button to start your webcam and record your message</p>
        </div>
      )
    }

    if (!stream.active) {
      return (
        <div className='trust-video-wrapper trust-video-msg'>
          <p>No video preview to show at the moment. Kindly hold on!!</p>
        </div>
      );
    }

    return (
      <div className="trust-video-container">
        <video
          className='trust-video preview-video'
          id='trustVideoPreview'
          ref={previewRef}
          autoPlay
        >
          <track kind='captions' />
        </video>
      </div>
    );
  }

  function VideoSrc() {
    if (videoUrl) {
      return (
        <div className="trust-video-container">
          <video className='trust-video' id='trustVideo' ref={videoRef} autoPlay>
            <source className='trust-video' src={videoUrl} type='video/mp4' />
            <source src={videoUrl} type='video/webm' />
            <track kind='captions' />
          </video>
        </div>
      );
    }

    return <Preview stream={previewStream} />;
  }

  return (
    <div className='trust-video-wrapper'>
      {VideoSrc()}
      {showControls &&
        <div className={`trust-video-controls ${controlsVisible && "show"}`}>
          <button
            type='button'
            className='trust-video-btn trust-video-control'
            onClick={handleMute}
          >
            {!isMuted ? <Volume /> : <Mute />}
          </button>

          {!preview && (
            <button
              type='button'
              className='trust-video-btn trust-video-play-btn trust-video-control paused'
              onClick={handlePlay}
            >
              {isPlaying ? <Pause /> : <Play />}
            </button>
          )}
          <div className='trust-video-progress' />
          {!preview && (
            <button
              type='button'
              className='trust-video-btn trust-video-control'
              onClick={fullScreen}
            >
              {" "}
              <Expand />{" "}
            </button>
          )}
        </div>

      }

      <section className='logo-video-section'>
        <div className='logo-video-position'>
          <Logo />
        </div>
      </section>
    </div>
  );
}

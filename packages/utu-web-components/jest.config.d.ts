export const roots: string[];
export const testRegex: string;
export const moduleFileExtensions: string[];
export const snapshotSerializers: string[];
export const setupFilesAfterEnv: string[];

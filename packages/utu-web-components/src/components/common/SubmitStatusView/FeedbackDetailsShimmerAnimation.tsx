import {h} from "preact";
import "./StatusView.scss";
import {SubmitStatus} from "../../../hooks/api";
import {NoVideoLogo, SadPenguin} from "../../../assets/Images";

export interface IFeedbackDetailsStatusViewProps {
  children: JSX.Element;
  submitStatus: SubmitStatus;
}

export default function FeedbackDetailsShimmerAnimation(props: IFeedbackDetailsStatusViewProps) {
  const {children, submitStatus} = props;

  switch (submitStatus) {
    case SubmitStatus.idle:
      return (
        <div className="placeholder-card_wrapper">
          <div className="placeholder-card">
            <div className="placeholder-icon"><SadPenguin/></div>
            <div className="placeholder-text-wrap">
              <div className="placeholder-text-title">Oops!  Signal not found.</div>
              <div className="placeholder-text-body">
                When people in your network leave reviews for this entity they will appear here.
              </div>
            </div>
          </div>
        </div>
      );
    case SubmitStatus.submitting:
      return (
        <div className="placeholder-card_wrapper">
          <div className="x-utu-description">
            <div className="x-utu-icon x-utu-skeleton"/>

            <div className="x-utu-details">
              <div className="x-utu-title x-utu-skeleton"/>
              <div className="x-utu-information x-utu-skeleton"/>
            </div>
          </div>

        </div>
      );
    case SubmitStatus.success:
      return children;
    default:
      return (
        <div className="placeholder-card_wrapper">
          <div className="placeholder-card">
            <div className="placeholder-icon"><SadPenguin/></div>
            <div className="placeholder-text-wrap">
              <div className="placeholder-text-title">Oops! Signal not found.</div>
              <div className="placeholder-text-body">
                              When people in your network leave reviews for this entity they will appear here.
              </div>
            </div>
          </div>
        </div>
      );
  }
}
import { ethers } from "ethers";
import { h } from "preact";
import { useEffect, useState } from "preact/hooks";
import { abi as UTTAbi } from "../../contracts/UTT.abi.json";
import { BaseComponent } from "../common/BaseComponent";
import { getDefaultNetworkName, getProvider, getUTTContractAddress, requestNetworkChange } from "../endorsements/EndorsementForm/networks";
import style from "./UttBalance.scss";
import LoadingIndicator from "../common/LoadingIndicator/LoadingIndicator";

export default function UttBalance() {
  const [UTTWalletState, setUTTWalletState] = useState<number | "N/A">("N/A");
  const [isLoading, setIsLoading] = useState(false);

  const fetchBalance = async () => {
    setIsLoading(true);
    try {
      const provider = await getProvider();
      const network = getDefaultNetworkName();
      const contractAddress = getUTTContractAddress(network);
      await requestNetworkChange(provider, network);
      const ethersProvider = new ethers.BrowserProvider(provider);
      const signer = await ethersProvider.getSigner();
      const userAddress = signer.getAddress()
      const getContract = new ethers.Contract(contractAddress, UTTAbi, ethersProvider);
      const walletUTTBalance = await getContract.balanceOf(userAddress);
      const UTTBalance = Number(ethers.formatUnits(walletUTTBalance, 0) || 0);
      setUTTWalletState(UTTBalance);
    } catch (e) {
      console.log(e);
      setUTTWalletState("N/A");
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(() => {
    fetchBalance();
  }, []);


  return (
    <BaseComponent style={style} className="utt_balance">
      {isLoading ? <LoadingIndicator width={15} height={15} /> : <h3 className="x-utu-h3">{UTTWalletState}</h3>}
    </BaseComponent>
  );
}
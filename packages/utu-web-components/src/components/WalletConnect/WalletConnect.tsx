import { h } from "preact";
import { ethers } from "ethers";
import style from "./WalletConnect.scss";
import {BaseComponent} from "../common/BaseComponent";
import {wallet} from "./WalletAddressContext"
import { useWalletConnectApi } from "../../hooks/api";

export default function WalletConnect() {
  const {web3Modal} = useWalletConnectApi();


  async function connectWallet() {
    const provider = await web3Modal.connect();
    const ethersProvider = new ethers.BrowserProvider(provider);
    const signer = await  ethersProvider.getSigner();
    const userAddress = signer.getAddress()
    wallet.address = await userAddress;
  }

  return (
    <BaseComponent style={style} className="x-utu-wallet-connect">
      <section className="x-utu-wallet-connect-section">
        <p className="x-utu-wallet-connect-text">
          See UTU trust signals from your network
        </p>
        <div>
          <button
            onClick={connectWallet}
            className="x-utu-wallet-text-input-btn x-utu-btn" type="submit">
            Connect Wallet
          </button>
        </div>
      </section>
    </BaseComponent>);
}
/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import PopUp from "../../common/PopUp";
import { BaseComponent } from "../../common/BaseComponent";
import FeedbackDetails from "../FeedbackDetails";
import { IFeedbackDetailsProps } from "../FeedbackDetails/FeedbackDetailsProps";
import style from "./FeedbackDetailsPopup.scss";
import { ATTR_BTN_COLOR, ATTR_THEME_COLOR } from "../../../names";
import Player from "../../video/RecordVideo/partials/Player";
import Logo from "../../../assets/Images"
// import Avatar from "../../screens/FeedbackDetails/Avatar";



export default function FeedbackDetailsPopup(props: IFeedbackDetailsProps) {

  const [popUpVisible, setPopUpVisible] = useState(false);

  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition = isDark ? "dark" : "light"

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  const [videoUrl, setVideoUrl] = useState("");

  const showVideo = (url: string) => {
    setVideoUrl(url)
    setVideoVisibility()
  }

  const setVideoVisibility = () => {
    setVideoVisible(!videoVisible)
    setPopUpVisible(!popUpVisible)

  }

  const [videoVisible, setVideoVisible] = useState(false);



  return <BaseComponent style={style}
    className="x-utu-feedback-details-popup x-utu-section">
    <button type="button"
      style={{ backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}` }}
      className={`x-utu-btn x-utu-btn-${envCondition} border-radius`} onClick={() => setPopUpVisible(true)}>
      Show Feedback Details
    </button>
    {popUpVisible &&
      <PopUp onClose={() => setPopUpVisible(false)} {...props}>
        <FeedbackDetails {...props} showVideo={showVideo} />
      </PopUp>
    }
    {videoVisible && videoUrl &&
      <PopUp closeButtonOverlay onClose={() => setVideoVisibility()}>
        <BaseComponent style={style}
          className="utu-feedback utu-feedback-video-popup">
          {
            // videoUrl, preview, previewStream, showControls
            Player(videoUrl, false, null, true)
          }
          <div className="logo-position-player">
            <Logo />
          </div>
        </BaseComponent>
      </PopUp>
    }
  </BaseComponent>;
}

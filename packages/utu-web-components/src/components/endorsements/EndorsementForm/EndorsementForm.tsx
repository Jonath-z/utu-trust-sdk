import { h } from "preact";
import { useState } from "preact/hooks";
import { useForm } from "react-hook-form";
import {  Contract, ethers, Transaction  } from "ethers";
import { BaseComponent, IBaseOwnProps } from "../../common/BaseComponent";
import { abi as UTTAbi } from "../../../contracts/UTT.abi.json";
import {
  ATTR_BTN_COLOR,
  ATTR_ENDORSEMENT_NETWORK,
  ATTR_TARGET_UUID,
  ATTR_THEME_COLOR,
  ATTR_TRANSACTION_ID,
  NETWORK_NAME
} from "../../../names";

import style from "./EndorsementForm.scss";
import { IFeedbackProps } from "../../screens/FeedbackForm/FeedbackProps";
import {
  addNetwork,
  requestNetworkChange,
  getDefaultNetworkName,
  getUTTContractAddress,
  getProvider
} from "./networks";
import {  UttBalance  } from "../../UttBalance";
import {  UttLogo  } from "../../../assets/Images";
import {resetSubmitStatusDelayed, SubmitStatus} from "../../../hooks/api";
import SubmitStatusView from "../../common/SubmitStatusView/SubmitStatusView";



export type IOwnProps = IBaseOwnProps;

export default function EndorsementForm(props: IFeedbackProps) {
  // environments
  const [isDark, setIsDark] = useState(false);
  const [txError, setTxError] = useState("");

  const [submitStatus, setSubmitStatus] = useState(SubmitStatus.idle);

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  // environment conditionals
  const envCondition = isDark ? "dark" : "light";

  const { register, formState: { errors }, handleSubmit } = useForm();
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  if (!(window?.ethereum || window?.web3 || window.utuWeb3Provider)
    || !props[ATTR_TARGET_UUID]
    || !ethers.isAddress(props[ATTR_TARGET_UUID])
    || !props[ATTR_TRANSACTION_ID]) return null;

  /**
	 * Determines which network to use for endorsements.
	 */
  function getEndorsementNetwork(): NETWORK_NAME {
    return props[ATTR_ENDORSEMENT_NETWORK] ?? getDefaultNetworkName();
  }

  /**
	 * Switches to the given network, adding it if necessary.
	 */
  async function switchNetwork(network: NETWORK_NAME) {
    const provider = getProvider();
    if (provider) {
      try {
        await requestNetworkChange(provider, network);
      } catch (e) {
        console.log(e, e.message);
        if (e.code === 4902 || e.message.toLowerCase().includes("unrecognized")) {
          await addNetwork(provider, network);
          await requestNetworkChange(provider, network);
        }
      }
    }

  }

  /**
	 * return the UTU contract instance on the given network
	 */
  async function getContractInstance(network: NETWORK_NAME): Promise<Contract> {
    const UTT_CONTRACT_ADDRESS = getUTTContractAddress(network);
    const web3Provider = new ethers.BrowserProvider(getProvider());
    const signer= await web3Provider.getSigner();
    return new ethers.Contract(UTT_CONTRACT_ADDRESS, UTTAbi, signer);
  }

  /**
	 * returns the current connected address
	 */
  async function getConnectedAddress(): Promise<string> {
    const web3Provider = new ethers.BrowserProvider(getProvider());
    const signer = await web3Provider.getSigner();
    return signer.getAddress();
  }

  // async function to prompt provider request wallet_permissions
  async function promptWalletAddressPermissions(): Promise<string[]> {
    const provider = getProvider()
    try {
      if (!provider) throw new Error("Provider not available. Please install MetaMask or similar extension.");

      const connectMessage = "Please connect to your wallet";
      console.log(connectMessage);

      const permissions = await provider.request({ method: "wallet_requestPermissions", params: [{ eth_accounts: {} }] });
      return permissions;
    } catch (error) {
      let errorMessage = error.message;

      // Append instructions for users who cancelled wallet dApp access requested.
      if (error.code === 4001)
        errorMessage += "\n\nYou did not grant the requested permissions. Please reload the page and try again.";

      console.error(errorMessage);
      throw new Error(errorMessage);
    }
  }
  const isWalletConnected = async (): Promise<boolean> => {
    const provider = getProvider()
    try {
      // Check if a Web3 provider (e.g. MetaMask) is installed
      if (!provider) {
        throw new Error("No Ethereum interface injected into browser");
      }

      // Use the provider to check if a wallet/account is connected to the current site
      const accounts = await provider.request({ method: "eth_accounts" });
      if (accounts.length === 0) {
        const permissions = await promptWalletAddressPermissions()
        if(permissions){
          return true
        }
        return false;
      }
      return true;
    } catch (error) {
      if (error.code === 4001 || error.code === 4100) {
        promptWalletAddressPermissions()
      } else {
        console.error(error);
      }
      return false;
    }
  }
  /**
	 * Creates an endorsement transaction
	 * @param address the target address of the endorsement
	 * @param amount the amount of UTT to stake on the endorsement
	 * @param transactionId the business transaction id (might or might not be an Ethereum tx id) to record with the
	 *      endorsement, if any
	 */
  async function endorse(
    address: string,
    amount: string,
    transactionId: string
  ): Promise<Transaction> {
    const network = getEndorsementNetwork();
    await switchNetwork(network);
    const contract = await getContractInstance(network);
    const connectedAddress = await getConnectedAddress();
    const utuBalance = await contract.balanceOf(connectedAddress);
    if (Number(utuBalance) < Number(amount)) {
      throw new Error("Insufficient UTU tokens");
    }
    const transaction = await contract.endorse(
      address,
      String(amount),
      transactionId
    );
    await transaction.wait();
    return transaction;
  }

  async function onSubmit({ amount }: { amount: string }) {
    if (!amount) return;
    setTxError("");
    try {
      setSubmitStatus(SubmitStatus.submitting);
      await isWalletConnected()
      await endorse(props[ATTR_TARGET_UUID], amount, props[ATTR_TRANSACTION_ID])
      setSubmitStatus(SubmitStatus.success);
      resetSubmitStatusDelayed(setSubmitStatus);
    } catch (error) {
      console.log(error);
      const errorMessage = error.data ? error.data.message : error.message;
      setTxError(errorMessage.length > 100 ? `${errorMessage.substring(0, 100)  }...` : errorMessage);
      setSubmitStatus(SubmitStatus.error);
      resetSubmitStatusDelayed(setSubmitStatus);
    }
  }

  const showError = ({ amount }: any) => {
    switch (amount?.type) {
      case "required":
        return "Supply a valid token value.";
      case "min":
        return "Minimum value supplied should be 1";
      case "integer":
        return "Value supplied must be an integer.";
      default:
        return "";
    }
  };

  return (
    <BaseComponent
      style={style}
      className={`endor-input-section x-utu-section x-utu-section-no-border-${envCondition}`}
      excludeBootstrap
      excludeFonts
    >
      <section className='endor-input-form-section'>
        <div className='endor-utt-balance'>
          <UttLogo />
          <div className='x-utu-utt-heading-text'>
            <h3 className='x-utu-h3'>UTT Balance</h3>
            <UttBalance />
          </div>
        </div>
        <div className={`endor-text endor-text-${envCondition}`}>
          <div className='endor-text-area'>
            <p className='endor-text-title'>Would you like to Endorse ?</p>
            <div className='endor-text-container'>
              <span className='endor-text-tooltip'>
								To endorse, you need to have some UTT in your account. Make sure
								you have a sufficient balance in your account
              </span>
              <p className='endor-text-info'>i</p>
            </div>
          </div>

          <p className='endor-text-body'>
						When you endorse a platform, it means you have personally had a good
						experience and want to back your rating by staking an amount of UTT
						(UTU trust token) on the platform.
          </p>
        </div>

        <form
          onSubmit={handleSubmit(onSubmit) as () => void}
          className='endor-input-form'
        >
          <div style={{ flex: 1, marginRight: 20 }}>
            <input
              className={`endor-input-form-text-area endor-input-form-text-area-${envCondition}`}
              {...register("amount",{required: true, minLength: 1 })}
              min='0'
              type='number'
              name='amount'
              placeholder='Your endorsement'
            />
            <div className="mt-1 x-utu-error">
              {errors.amount && `* ${showError(errors)}`}
            </div>
            {
              txError && <div className="mt-1 x-utu-error">{txError}</div>
            }
          </div>
          <div>
            <SubmitStatusView submitStatus={submitStatus}>
              <button
                style={{ backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}` }}
                className={`endor-btn x-utu-btn x-utu-btn-${envCondition}`} type="submit">Endorse
              </button>
            </SubmitStatusView>
          </div>
        </form>
      </section>
    </BaseComponent>
  );
}

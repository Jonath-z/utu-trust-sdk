/* eslint-disable no-nested-ternary */
import {h} from "preact";
import {useState} from "preact/hooks";
import ReactStars from "react-stars";
import {BaseComponent} from "../../common/BaseComponent";
import {IFeedbackDetailsProps} from "../../screens/FeedbackDetails/FeedbackDetailsProps";
import {ATTR_THEME_COLOR} from "../../../names";
import style from "./StarRatingShow.scss";
import FeedbackDetailsShimmerAnimation from "../../common/SubmitStatusView/FeedbackDetailsShimmerAnimation";
import Star from "../../../assets/Images/star-solid";
import {NoBadgeLogo} from "../../../assets/Images";

export default function StarRatingShow(props?: IFeedbackDetailsProps) {

  // environment conditionals
  const [isDark, setIsDark] = useState(false);
  const envCondition = isDark ? "dark" : "light"
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  const {avg, summaryText} = props.feedbackSummary?.stars ?? {};

  return (
    <FeedbackDetailsShimmerAnimation submitStatus={props.submitStatus}>
      <BaseComponent style={style}>
        {!props.feedbackSummary?.stars || false?

          <div className="placeholder-card">
            <div className="placeholder-icon"><Star {...props} /></div>
            <div className="placeholder-text-wrap">
              <div className="placeholder-text-title">No star ratings available</div>
              <div className="placeholder-text-body">
                  When people in your network leave star ratings for this entity they will appear here.
              </div>
            </div>
          </div>
          :
          <section className="stars-section">
            <p><b>star ratings from people in your network</b></p>
            <div className="stars-section-align">
              <ReactStars
                count={5}
                value={avg}
                size={30}
                color2="#FFDD33"
                edit={false}
                className="pr-4"
              />
            </div>
            <div className="stars-section-text"><p className="stars-section-text-content">{summaryText}</p></div>
          </section>

        }
      </BaseComponent>
    </FeedbackDetailsShimmerAnimation>
  );
}

/* eslint-disable no-nested-ternary */
import {h} from "preact";
import {useState} from "preact/hooks";
import {useReactMediaRecorder} from "react-media-recorder";
import PopUp from "../../common/PopUp";
import {BaseComponent} from "../../common/BaseComponent";
import FeedbackForm from "../FeedbackForm/FeedbackForm";
import {IFeedbackProps} from "../FeedbackForm/FeedbackProps";
import style from "../FeedbackForm/FeedbackForm.scss";
import {ATTR_BTN_COLOR, ATTR_THEME_COLOR} from "../../../names";
import Recorder from "../../video/RecordVideo/Recorder";

export default function FeedbackFormPopup(props: IFeedbackProps) {
  const [popUpVisible, setPopUpVisible] = useState(false);
  
  // environments
  const [isDark, setIsDark] = useState(false);
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  // environment conditionals
  const envCondition = isDark ? "dark" : "light"

  // const [isRecording, setIsRecording] = useState(false)

  // const { stopRecording } = useReactMediaRecorder({
  //   audio: true,
  //   timeslice: 1000,
  //   video: false
  // })



  const setVideoVisibility = () => {
    setVideoVisible(!videoVisible)
    setPopUpVisible(!popUpVisible)

  }



  const [videoVisible, setVideoVisible] = useState(false);


  return <BaseComponent style={style}
    className="x-utu-feedback-form-popup x-utu-section">
    <button type="button"
      style={{backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}`}}
      className={`x-utu-btn x-utu-btn-${envCondition} border-radius`} onClick={() => setPopUpVisible(true)}>
          Give feedback
    </button>
    {popUpVisible && (
      <PopUp onClose={() => setPopUpVisible(false)} {...props}>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <FeedbackForm {...props} setVideoVisibility={setVideoVisibility} />
      </PopUp>
    )}
    {videoVisible && (
      <PopUp closeButtonOverlay onClose={setVideoVisibility}>
        <Recorder {...props} />
      </PopUp>
    )}
  </BaseComponent>
}

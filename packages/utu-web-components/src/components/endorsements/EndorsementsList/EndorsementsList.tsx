/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent, IBaseOwnProps, config } from "../../common/BaseComponent";
import { IFeedbackDetailsProps } from "../../screens/FeedbackDetails/FeedbackDetailsProps";
import style from "./EndorsementsList.scss";
import {IEndorsements, IEntity} from "../../../hooks/api";
import FeedbackDetailsShimmerAnimation from "../../common/SubmitStatusView/FeedbackDetailsShimmerAnimation";
import {NoBadgeLogo, Stamp} from "../../../assets/Images";
import {
  ATTR_THEME_COLOR,
  ATTR_SOURCE_UUID
} from "../../../names";


export type IOwnProps = IBaseOwnProps;
// const minLength = 5;

const CONNECTION_TYPE_REF: Record<string, string> = {
//  twitter: "Your Twitter connection",
//  telegram: "Your Telegram contact"
  twitter: "Twitter user",
  telegram: "Telegram user"
}

const DEFAULT_CONNECTION_REF = "User";

interface ApiConnectionData {
  name: string | null,
  image?: string
}

interface ConnectionInfo {
  ref: string;
  name?: string;
}

function getConnectionInfo(entity: IEntity) : ConnectionInfo {
  if(!entity) return { ref: "Someone" };

  // eslint-disable-next-line no-restricted-syntax
  for(const key in CONNECTION_TYPE_REF) {
    if(entity.properties[key]) {
      return {
        ref: CONNECTION_TYPE_REF[key],
        name: (entity.properties[key] as ApiConnectionData).name
      }
    }
  }

  // No connection type was found, return default
  return { ref: DEFAULT_CONNECTION_REF,  name: entity.name }
}

const SELF_INFO : ConnectionInfo = {
  ref: "You"
}

function getEndorsementTextBody(userUuid: string, endorsementInfo: IEndorsements) {
  const endorser = endorsementInfo.source;
  const isSelf = endorser.uuid === userUuid;
  const connectionInfo = isSelf ? SELF_INFO : getConnectionInfo(endorser);
  const has = isSelf ? "have" : "has";

  return (<p className="endor-text-contacts-body ">
    {connectionInfo.ref}&nbsp;
    { connectionInfo.name?.length > 0 &&
      <span className="endor-text-truncate"><b>{connectionInfo.name}</b>&nbsp;</span>
    }
    {has} endorsed <b className="mx-1">{ endorsementInfo.endorsement?.value } UTT</b>
  </p>);
}

export default function EndorsementsList(props: IFeedbackDetailsProps) {
  // environments
  const [isDark, setIsDark] = useState(false);

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"


  const {endorsements} = props.feedbackSummary || {};
  const displayEndorsements = endorsements?.slice(0, config.feedbackDetailsMaxEndorsements);

  // image
  const img: any = displayEndorsements?.map(i => i.source?.image);
  const endorImg = ()=>{
    return <img
      className={`endor-text-image-item endor-text-image-item-${envCondition}`}
      src={img}
      alt="endorsment"
      onError={e => {
        (e.target as HTMLImageElement).onerror = null;
        (e.target as HTMLImageElement).src = "https://via.placeholder.com/150/FFFF00/000000/?text=0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";
        (e.target as HTMLImageElement).className = "endor-text-image-item";
        (e.target as HTMLImageElement).alt = "";
      }}
    />
  }

  return  (
    <FeedbackDetailsShimmerAnimation submitStatus={props.submitStatus}>
      <BaseComponent
        style={style}
        className="x-utu-feedback-details-endorsements"
        excludeBootstrap excludeFonts>
        <p> <b> endorsements from people in your network </b></p>
        <section className="x-utu-feedback-details-endorsements-wrapper">
          {/* <h3 className="x-utu-h3 endor-title">endorsements</h3> */}
          {!endorsements || false?

            <div className="placeholder-card">
              <div className="placeholder-icon"><Stamp {...props} /></div>
              <div className="placeholder-text-wrap">
                <div className="placeholder-text-title">No endorsements available</div>
                <div className="placeholder-text-body">
                    When your people in your network endorse this entity they will appear here.
                </div>
              </div>
            </div>
            :

            displayEndorsements.map((i: IEndorsements) => (
              <div className="x-utu-feedback-details-endorsements-body">
                {endorImg()}
                <div className="x-utu-feedback-details-endorsements-body_text">
                  {getEndorsementTextBody(props[ATTR_SOURCE_UUID], i)}
                </div>
              </div>
            ))
          }
        </section>
   
      </BaseComponent>
    </FeedbackDetailsShimmerAnimation>)
}

import express from "express";
import cors from 'cors';
import { Server, FileStore, EVENTS } from "tus-node-server";
const tusServer = new Server();

tusServer.datastore = new FileStore({
  path: "/files",
  namingFunction: () => `${Math.random().toString(36).substring(7)}.mp4`
});

tusServer.on(EVENTS.EVENT_UPLOAD_COMPLETE, ({ file }) => console.log(`${file.id} was uploaded`));

const app = express();
const corsOptions = {origin: "*", credentials: true};
app.use(cors(corsOptions));

app.use("/", express.static("public"));
app.use("/tus-js-client", express.static("../../node_modules/tus-js-client/dist"));
app.use("/uploads/files", express.static("files"));

const uploadApp = express();

uploadApp.all("*", tusServer.handle.bind(tusServer));
app.use("/uploads", uploadApp);

const port = 2080;
app.listen(port, () => console.log(`Listening on port ${port}`));

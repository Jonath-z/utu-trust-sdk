import {IBaseOwnProps} from "../../common/BaseComponent";
import {
  ATTR_BTN_COLOR,
  ATTR_SOURCE_UUID,
  ATTR_TARGET_HUMAN_READABLE,
  ATTR_TARGET_TYPE,
  ATTR_TARGET_UUID,
  ATTR_THEME_COLOR,
  TAG_FEEDBACK_DETAILS_POPUP
} from "../../../names";
import {IFeedback, SubmitStatus} from "../../../hooks/api";

export interface IFeedbackDetailsProps extends IBaseOwnProps {
  [ATTR_SOURCE_UUID]: string;
  [ATTR_TARGET_UUID]: string;
  [ATTR_BTN_COLOR]: string;
  [ATTR_THEME_COLOR]: string;
  [TAG_FEEDBACK_DETAILS_POPUP]: string;
  [ATTR_TARGET_TYPE]: string
  [ATTR_TARGET_HUMAN_READABLE]: string
  setVideoVisibility?: () => void
  showVideo?: (url: string) => void


  // for internal use:
  title: any;
  feedbackSummary: IFeedback | undefined;
  submitStatus: SubmitStatus
}

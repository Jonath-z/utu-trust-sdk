import axios, {AxiosRequestConfig} from "axios";
import qs from "qs";
import {ethers} from "ethers";
import {
  API_FEEDBACK, API_FEEDBACK_SUMMARY,
  API_RANKING,
  API_VERIFY_ADDRESS,
  CORE_API_BASE,
  UTU_API_BASE_URL_PRODUCTION,
  UTU_API_BASE_URL_TEST
} from "../names";

import {config} from "../components";
import {getProvider} from "../components/endorsements/EndorsementForm/networks";
import {AuthData} from "../hooks/auth";

import {IFeedbackData} from "./models";

function createEntityCriteria(uuid: string) {
  return {ids: {uuid}};
}

function withAuthorizationHeader(accessToken: string, config: AxiosRequestConfig = {}) {
  return {
    ...config,
    headers: {
      ...(config.headers || {}),
      ...getAuthorizationHeader(accessToken)
    }
  };
}

function getAuthorizationHeader(accessToken: string) {
  return {"Authorization": `Bearer ${accessToken}`};
}

/**
 * Use the UTU API to post feedback.
 * @param apiUrl
 * @param targetType
 * @param accessToken
 * @param sourceUuid sourceUuid your entity's uuid e.g. wallet address of logged in person
 * @param targetUuids targetUuid unique identifier e.i uuid
 * @return a promise which will resolve with an unspecified value when the feedback was posted successfully, and will reject otherwise.
 * @see IFeedbackData
 */
export async function getRanking(apiUrl :string,
  sourceUuid: string,
  targetType: string,
  accessToken: string,
  targetUuids?: string[]){
  const queryParams = qs.stringify({
    sourceCriteria: JSON.stringify(createEntityCriteria(sourceUuid)),
    targetType,
    targetCriterias: targetUuids?.map(uuid => JSON.stringify(createEntityCriteria(uuid)))
  });
  const url = `${apiUrl}${CORE_API_BASE}${API_RANKING}?${queryParams}`;
  const axiosRequestConfig: AxiosRequestConfig = withAuthorizationHeader(accessToken);
  const response = await axios.get<any>(url, axiosRequestConfig );
  return response;
}

/**
 * Use the UTU API to post feedback.
 * @param apiUrl
 * @param accessToken
 * @param sourceCriteria
 * @param targetCriteria
 * @param transactionId
 * @param feedbackData
 * @return a promise which will resolve with an unspecified value when the feedback was posted successfully, and will reject otherwise.
 * @see IFeedbackData
 */
export async function coreSDKsendFeedback(apiUrl: string,
  accessToken: string,
  sourceCriteria: { ids: { uuid: string; }; },
  targetCriteria: { ids: { uuid: string; }; },
  transactionId: string,
  feedbackData: IFeedbackData
){
  return axios.post(
    `${apiUrl}${CORE_API_BASE}${API_FEEDBACK}`,
    {
      sourceCriteria,
      targetCriteria,
      transactionId,
      items: feedbackData
    },
    withAuthorizationHeader(accessToken)
  ).then(result => {
    return result;
  }).catch(error => {
    return error;
  });
}

/**
 * Get source entity's feedback on target entity from the UTU API.
 * @param apiUrl
 * @param accessToken authentication token
 * @param sourceUuid sourceUuid your entity's uuid e.g. wallet address of logged in person
 * @param targetUuid targetUuid unique identifier e.i uuid
 * @return an object with the feedback summary and the submit status
 * @see IFeedback
 * @see SubmitStatus
 */
export async function coreSDKGetFeedbackDetails(apiUrl: string,
  accessToken: string,
  sourceUuid: string,
  targetUuid: string
){
  const sourceCriteria = createEntityCriteria(sourceUuid);
  const targetCriteria = createEntityCriteria(targetUuid);
  
  const queryParams = qs.stringify({
    // sourceCriteria, don't send for now, there's a bug in the API which then only returns feedback from that
    // source, but not from their connections
    sourceCriteria, targetCriteria
  });
  
  const url = `${apiUrl}${CORE_API_BASE}${API_FEEDBACK_SUMMARY}?${queryParams}`;
  return axios.get<any>(url, withAuthorizationHeader(accessToken))
}



export async function signAndGetToken(overrideApiUrl: string | null = null,
  walletProvider: any = null,
  cookies = false
): Promise<AuthData | null> {

  // @ts-ignore.
  if (window?.xUtuConfig?.production){
    // @ts-ignore
    config.production = window.xUtuConfig.production as boolean;
  } else {
    // @ts-ignore
    window.xUtuConfig = {}
    // @ts-ignore
    xUtuConfig.production = true;
  }

  const utuApiBase = overrideApiUrl ?? (config.production ? UTU_API_BASE_URL_PRODUCTION : UTU_API_BASE_URL_TEST);
  const _provider = getProvider();
  // @ts-ignore
  if (!_provider && !walletProvider) return null;
  // @ts-ignore
  const provider = new ethers.BrowserProvider(walletProvider || _provider);
  const signer = await provider.getSigner();
  const address = await signer.getAddress();
  const signature = await signer.signMessage("Sign in at UTU");

  const { data } = await axios.post(
    `${utuApiBase}${API_VERIFY_ADDRESS}`,
    {
      address,
      signature
    },
    {
      withCredentials: !!cookies
    }
  );

  return data
  
}
/* eslint-disable no-nested-ternary */
import { Fragment, h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent } from "../../common/BaseComponent";
import {
  ATTR_THEME_COLOR
} from "../../../names";
import { IFeedbackDetailsProps } from "../../screens/FeedbackDetails/FeedbackDetailsProps";
import style from "./ReviewShow.scss";
import Avatar from "../../screens/FeedbackDetails/Avatar";
import FeedbackDetailsShimmerAnimation from "../../common/SubmitStatusView/FeedbackDetailsShimmerAnimation";
import {NoBadgeLogo} from "../../../assets/Images";


export default function BadgesShow(props: IFeedbackDetailsProps) {

  // environment conditionals
  const [isDark, setIsDark] = useState(false);
  const envCondition =
   isDark ? "dark" : "light"
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  // text reviews
  const textReview = props.feedbackSummary?.reviews

  return (
    <FeedbackDetailsShimmerAnimation submitStatus={props.submitStatus}>
      <BaseComponent
        style={style}
        className="review-section"
        excludeBootstrap
        excludeFonts
      >
        <p><b> text from people in your network</b></p>
        {!textReview || false || textReview.length <= 0 ?
          <div className="placeholder-card">
            <div className="placeholder-icon"><Avatar {...props} /></div>
            <div className="placeholder-text-wrap">
              <div className="placeholder-text-title">No text reviews available</div>
              <div className="placeholder-text-body">
                            When people in your network leave text reviews for this entity they will appear here.
              </div>
            </div>
          </div>
          :
          <section className="review-card-section">
            {
              textReview.map((rev: any) => (
                <div className="review-card">

                  <div style={{width: "30rem"}} className="review-card-group">
                    {rev.image ? <img className="review-img" src={rev.image}
                      onError={e => {
                        (e.target as HTMLImageElement).onerror = null;
                        (e.target as HTMLImageElement).src = "";
                        (e.target as HTMLImageElement).className = "";
                        (e.target as HTMLImageElement).alt = "";
                      }} alt="person face"/> :
                      <div className="ml-5 video-avatar"><Avatar {...props} /></div>}
                    <div className="review-text">
                      <div
                        className={`review-text-sum review-text-${envCondition}`}>{rev.summaryText}</div>
                      <div className={`review-text-cont review-text-${envCondition}`}>
                        <i> {rev.content} </i></div>
                    </div>
                  </div>
                </div>
              ))
            }
          </section>
        }
      </BaseComponent>
    </FeedbackDetailsShimmerAnimation>
  );
}

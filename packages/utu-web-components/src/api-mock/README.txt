====================================================================================================
README.txt
====================================================================================================

This file descrives how to use the mock-api

====================================================================================================
Using api-mock with Chrome Extension
====================================================================================================

(i) In the chrome extension change env.dev to look like:

    import { polygonMumbai } from 'wagmi/chains'

    export default {
      // apiUrl: 'https://api.stage.ututrust.com',
      // utuUrl: 'https://stage-app.utu.io',
      apiUrl: 'http://localhost:3000',
      utuUrl: 'http://localhost:3000',
      chain: polygonMumbai,
      chainName: 'polygon_mumbai'
    };

(ii) cd to:

    utu-trust-sdk/packages/utu-web-components

  Run:

    npm install

(iii) Then run:

    npm run api-mock2

  or run:

    node src/api-mock/index.js

  
====================================================================================================
Using api-mock without Chrome Extension
====================================================================================================

(i) cd to:

    utu-trust-sdk/packages/utu-web-components

  Run:

    npm install

(ii) The run:

    npm run api-mock


====================================================================================================
To generate tokens
====================================================================================================

I generated a token here:

  https://www.scottbrady91.com/tools/jwt

And I debug tokens here:

  https://jwt.io/
import { h } from "preact";
import style from "../../video/RecordVideo/partials/Spinner/Spinner.scss"

interface LoadingIndicatorProps {
  width?: number;
  height?: number;
  useFilter?: boolean;
}


const defaultProps = {
  width: 20,
  height: 20,
  useFilter: false
};


function LoadingIndicator({ width = 90, height = 90, useFilter = false }: LoadingIndicatorProps) {
  return (
    <svg
      style={style}
      className="x-utu-spinner"
      id="goo-loader"
      width={width}
      height={height}
      aria-label="loading"
    >
      {useFilter && (
        <filter id="fancy-goo">
          <feGaussianBlur in="SourceGraphic" stdDeviation="6" result="blur" />
          <feColorMatrix
            in="blur"
            mode="matrix"
            values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9"
            result="goo"
          />
          <feComposite in="SourceGraphic" in2="goo" operator="atop" />
        </filter>
      )}
    </svg>
  );
}

LoadingIndicator.defaultProps = defaultProps;

export default LoadingIndicator;
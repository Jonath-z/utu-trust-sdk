/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent } from "../../common/BaseComponent";
import {
  ATTR_THEME_COLOR
} from "../../../names";
import Avatar from "../../screens/FeedbackDetails/Avatar";
import { IFeedbackDetailsProps } from "../../screens/FeedbackDetails/FeedbackDetailsProps";
import style from "./VideoShow.scss";
import FeedbackDetailsShimmerAnimation from "../../common/SubmitStatusView/FeedbackDetailsShimmerAnimation";
import {NoVideoLogo} from "../../../assets/Images";



// component for everything inclusing profile images, video or undefined
function UserImageVideo({ url, image, showVideo, ...props }: any) {

  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition =
     isDark ? "dark" : "light"
  
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }
  

  return (
    <div>
      <div  className="avatar" onClick={() => showVideo(url)}>
        {!image || image.length === 0 ?<div>
          <div style={{width: "2rem", margin: "auto"}} className={`avatar-no-img avatar-no-img-${envCondition}`}>
            <Avatar {...props} />
          </div>
        </div>
          :     <li className="avatar-img-top-list-item" key={image}> <img
            className={`avatar-img-top-details rounded-circle summary-image-item summary-image-item-${envCondition}`}
            src={image}
            alt="profile"
          /> </li>
        }
      </div>
    </div>
  )
}

export default function VideoShow(props: IFeedbackDetailsProps) {
  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition =
   isDark ? "dark" : "light"

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }


  const { videos } = props.feedbackSummary?.videos ?? {};

  const profileImage = videos?.map(img => img.image) ||
      <div className={`ml-5 no-data-avatar avatar-no-img-${envCondition}`}><Avatar {...props} /></div>;

  const profileUrl = videos?.map(img => img.url) ||
      <div className={`ml-5 no-data-avatar avatar-no-img-${envCondition}`}><Avatar {...props} /></div>;

  const profileText = props.feedbackSummary?.videos?.summaryText;


  // set of profile images 
  // const videoImg = videos instanceof Array ? videos.map(img => (
  //   <li className="avatar-img-top-list-item" key={img.image}>
  //     <img
  //       className={`avatar-img-top-details rounded-circle summary-image-item summary-image-item-${envCondition}`}
  //       src={img.image}
  //       alt="profile"
  //     />
  //   </li>
  // ) ) : "";

  

  return (
    <BaseComponent
      style={style}
    
      excludeBootstrap
      excludeFonts
    >
      <FeedbackDetailsShimmerAnimation submitStatus={props.submitStatus}>

      
        {!videos || false || videos.length <= 0 ?

          <div className="placeholder-card">
            <div className="placeholder-icon"><NoVideoLogo {...props} /></div>
            <div className="placeholder-text-wrap">
              <div className="placeholder-text-title">No video reviews available</div>
              <div className="placeholder-text-body">
                      When people in your network leave video reviews for this entity they will appear here.
              </div>
            </div>
          </div>
          :
          <div   className="video-section">
            <section
              className="avatar-img-top-video">
              <p> <b>video from people in your network </b></p>
              <div className="avatar-img-top-video-container">
                <div>
                  <div className="avatar-img-top-video-profile">
                    {profileImage ? videos.map(i => (
                      <UserImageVideo url={i.url} image={i.image} showVideo={props.showVideo} {...props}/>)) :
                      <img
                        className={`avatar-img-top-details rounded-circle summary-image-item summary-image-item-${envCondition}`}
                        src="https://utu.io/wp-content/uploads/2021/07/P1.png"
                        alt="profile"
                      />
                    }
                  </div>
                  <div className="avatar-img-top-video-text-container">
                    <p className="avatar-img-top-video-text">{profileText || "no video reviews available"} </p>
                  </div>
                </div>

              </div>
            </section>
          </div>
        }


      </FeedbackDetailsShimmerAnimation>
    </BaseComponent>
  );
}



/* eslint-disable no-nested-ternary */
import {ComponentChildren, h} from "preact";
import {useState} from "preact/hooks";
import {BaseComponent} from "../BaseComponent";
import style from "./PopUp.scss";

export interface IOwnProps {
  onClose: () => void;
  children?: ComponentChildren;
  closeButtonOverlay?: boolean;
  themeColor?: string;
}

export default function PopUp({ onClose, children, closeButtonOverlay,themeColor}: IOwnProps) {

  // environments
  const [isDark, setIsDark] = useState(false);

  if (themeColor === "dark") {
    setIsDark(true);
  }

  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"

  return <BaseComponent style={style} className="x-utu-popup">
    <div className={`x-utu-popup-container x-utu-popup-container-${envCondition}`} >
      <div className="x-utu-popup-container-content">
        <button
          type="button"
          className={`x-utu-popup-icon-btn x-utu-popup-icon-btn-${envCondition} ${closeButtonOverlay && "x-utu-popup-icon-btn-overlay"}`}
          onClick={onClose}>
          &times;
        </button>
      </div>
      {
        children
      }
    </div>
    <div onClick={onClose} className="x-utu-popup-background" />

  </BaseComponent>;
}

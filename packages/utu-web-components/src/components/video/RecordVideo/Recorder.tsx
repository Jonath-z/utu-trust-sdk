/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useContext, useEffect, useState } from "preact/hooks";
import {
  IVideoStorage, StatusMessages, useReactMediaRecorder
} from "react-media-recorder";
import { ReadableStorage } from "react-media-recorder-readable-storage";
// import Loader from "react-loader-spinner";
import { useFeedbackApi, useUploadVideoApi } from "../../../hooks/api";
import {
  ATTR_SOURCE_UUID,
  ATTR_TARGET_UUID, ATTR_THEME_COLOR,
  ATTR_TRANSACTION_ID,
  TAG_FEEDBACK_FORM
} from "../../../names";
import { BaseComponent, getBaseProps } from "../../common/BaseComponent";
import { IFeedbackProps } from "../../screens/FeedbackForm/FeedbackProps";
import Logo from "../../../assets/Images";
import Player from "./partials/Player";
import Spinner from "./partials/Spinner/Spinner";
import style from "./RecordVideo.scss";
import RecordIcon from "./icons/recordIcon";
import { WalletAddressContext } from "../../WalletConnect/WalletAddressContext";

// type Props = IFeedbackProps & {
//   // onClose: () => void
// }

// const mediaStream = new MediaStream();

export default function Recorder(props: IFeedbackProps) {
  /**
     * Holds the active reader retrieved from ReadableStorage.getReader() while a video is recording, or null while no
     * video is recording.
     */
  const [activeReader, setActiveReader] = useState(null);
  const [videoUploading, setVideoUploading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [readyVideoUrl, setReadyVideoUrl] = useState(null);
  const [hideControls, setHideControls] = useState(true);



  // environments
  const [isDark, setIsDark] = useState(false);

  const walletAddress = useContext(WalletAddressContext);
  if (props[ATTR_SOURCE_UUID] === "address") {
    props[ATTR_SOURCE_UUID] = walletAddress;
  }

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  const { apiUrl } = getBaseProps(props, TAG_FEEDBACK_FORM);

  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"


  // const constraints = {
  //   // set user env var
  //   height: 810,
  //   width: 432,
  //   frameRate: 24,
  //   facingMode: "user"
  // };

  // Handle upload status from upload api
  const handleVideoUploadStatus = (videoUrl: string | null) => {
    if (videoUrl) {
      setReadyVideoUrl(videoUrl);
    }
  }

  // Upload a readable stream returns a status object with various properties
  const { publishedVideoUrl, successMessage, errorMessage } = useUploadVideoApi(apiUrl, activeReader, handleVideoUploadStatus);

  // Initialize the required api feedback call and get the sendFeedback method for posting new feedback
  const { sendFeedback } = useFeedbackApi(
    apiUrl,
    props[ATTR_SOURCE_UUID],
    props[ATTR_TARGET_UUID],
    props[ATTR_TRANSACTION_ID]
  );

  // Send a call with video related feedback and listen for changes to the url or send feedback method.

  useEffect(
    () => {
      console.count("sendFeedbackOnce");

      if (readyVideoUrl) {
        sendFeedback({ video: readyVideoUrl })
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [readyVideoUrl]
  );


  const VideoSrc = (status: StatusMessages, previewStream: MediaStream) => {
    if (publishedVideoUrl) {
      // return Player(publishedVideoUrl, false);
      // videoUrl, preview, previewStream, showControls
      return Player(publishedVideoUrl, false, null, true);
      console.log("status videosrc", status)
    }

    return Player(null, true, previewStream);
  };



  function videoStorageFactory(): IVideoStorage {
    const storage = new ReadableStorage();
    setActiveReader(storage.getReader());
    return storage;
  }


  const { status,
    startRecording,
    stopRecording,
    previewStream } = useReactMediaRecorder({
    audio: true,
    video: true,
    videoStorageFactory,
    timeslice: 1000
  })

  useEffect(
    () => {
      console.log("hide controls");
      if(isLoading) {
        setHideControls(false)
      
      }
      return () => {
        stopRecording()
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const isLoading = loading || ["stopping", "acquiring_media"].includes(status)

  // const isReady = (status === "idle" || status === "stopped") && !isLoading;

  const isInactive = status === "idle"

  // const isStopped = status === "stopped" 

  const isRecording = status === "recording";

  const onStop = () => {
    setLoading(true);
    stopRecording();
    setActiveReader(null);
    setVideoUploading(!videoUploading)
    setHideControls(false)
    console.log("status_onstop", status)
  }


  const onRecord = () => {
    startRecording()
  }

  console.log("status_onrecording", status)



  // useEffect(() => {
  //   getMediaStream();
  //   return () => {
  //     stopStream()
  //   }
  // }, [])

  return (
    <BaseComponent style={style}
      className="x-utu-video-screen-recording d-flex flex-column align-items-center justify-content-center">
      {VideoSrc(status, previewStream)}

      {isRecording && (
        <button
          type="button"
          className={`x-utu-video-btn-round-stop x-utu-video-btn-round-stop-${envCondition} x-utu-video-btn-round btn-round-${envCondition}`}
          onClick={onStop}
        >
          <div className={`x-utu-video-btn-stop-icon x-utu-video-btn-stop-icon-${envCondition}`} />
        </button>
      )}

      {isInactive && (!publishedVideoUrl) && (
        <button
          type="button"
          className={`
                    x-utu-video-btn-round-stop x-utu-video-btn-round-stop-${envCondition} x-utu-video-btn-round x-utu-video-btn-round-${envCondition}
                  `}
          onClick={
            onRecord
          }
        >
          <RecordIcon />
        </button>
      )}

      {isLoading && (!publishedVideoUrl) &&
        <button
          type="button"
          className={`x-utu-video-btn-round-loading x-utu-video-btn-round x-utu-video-btn-round-${envCondition}`}
        >
          <div className={`mb-2 mr-2 x-utu-video-btn-round-spinner x-utu-video-btn-round-spinner-${envCondition}`}>
            <Spinner />
          </div>
        </button>
      }

      <div className="video-msg d-flex align-items-center">
        {successMessage && <h3 className="x-utu-video-response-message my-4 text-white bg-success"> {successMessage} </h3>}
        {errorMessage && <h3 className="x-utu-video-response-message my-4 text-white bg-danger"> {errorMessage} </h3>}
      </div>

      <section className="logo-position-video">
        <Logo />
      </section>
    </BaseComponent>

  );
}

import { h } from "preact";
import { useEffect, useRef } from "preact/hooks";
import { IRankingItem, useRankingApi } from "../../hooks/api";
import { BaseComponent, getBaseProps, IBaseOwnProps } from "../common/BaseComponent";
import {TAG_ROOT, ATTR_TARGET_UUIDS, ATTR_TARGET_TYPE, ATTR_SOURCE_UUID } from "../../names";

import style from "./Root.scss";

export interface IOwnProps extends IBaseOwnProps {
  [ATTR_SOURCE_UUID]: string;
  [ATTR_TARGET_TYPE]: string;
  [ATTR_TARGET_UUIDS]?: string;
}

export interface IDataElement extends HTMLDivElement {
  data: IRankingItem[];
}

export default function Root(props: IOwnProps) {
  const { apiUrl } = getBaseProps(props, TAG_ROOT);
  const sourceUuid = props[ATTR_SOURCE_UUID];
  const targetType = props[ATTR_TARGET_TYPE];
  const targetUuids =  props[ATTR_TARGET_UUIDS];

  const { rankingItems } = useRankingApi(apiUrl, sourceUuid, targetType, targetUuids);
  const ref = useRef<IDataElement>();

  useEffect(() => {
    if (ref.current && rankingItems) {
      ref.current.data = rankingItems;

      setTimeout(() => {
        ref.current.dispatchEvent(
          new CustomEvent("loaded", {
            composed: true,
            detail: { data: rankingItems }
          })
        );
      }, 0);
    }
  }, [ref, rankingItems]);

  return (
    <BaseComponent
      forwardedRef={ref}
      className="recommendation-root"
      style={style}
      excludeBootstrap
      excludeFonts
    >
      <slot />
    </BaseComponent>
  );
}

/**
 * Takes a string containing a space-separated list of ids and transforms it into an array of (string) ids.
 * @param spaceSeparatedtargetUuids
 */
function parseTargetUuids(spaceSeparatedtargetUuids?: string) : string[] {
  const trimmed = spaceSeparatedtargetUuids?.trim();
  // don't split an empty string (which weirdly creates an array with one empty string element)
  return trimmed ? trimmed.split(" ") : undefined;
}

const jsonServer = require("json-server");

const server = jsonServer.create();
const router = jsonServer.router("src/api-mock/db.json");
const middlewares = jsonServer.defaults();

server.use(jsonServer.bodyParser);
server.use(middlewares);

const getVerifyJson = () => {
  return {
    "access_token": "eyJ0eXAiOiJhdCtqd3QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImIzZWU3NzY1MWFkNjM3ZTBmODllNDdkM2QwZmI3OGJhIn0.eyJpc3MiOiJodHRwczovL2lkcC5sb2NhbCIsImF1ZCI6ImFwaTEiLCJzdWIiOiI1YmU4NjM1OTA3M2M0MzRiYWQyZGEzOTMyMjIyZGFiZSIsImNsaWVudF9pZCI6Im15X2NsaWVudF9hcHAiLCJleHAiOjI2ODAwOTg0NzUsImlhdCI6MjY4MDA5NDg3NSwianRpIjoiNTZjYzg2NWI2YmFlYWZjNDBjZTI2NmY3YjIxMjA2YWYifQ.TAcFR4iqvhjZHBrNdfS6YK6CM7mMaQonVo4UpDfl3BFO_ZPrsO1DAE8NHBfVaEx19dJ0gmDqa3swj5eOe4w44vb_ecZACAw9gJCIyzsvVbtVa_Jcie6OE-FHpXPfPm26Nx5tbSu-_IlyB7-dvSiqN2-SNGHXmcAk8T55eyKXQE63MljgVU7iF9eGZpMDyKmN5SjvthuKxzRd1Tm_pm-nJ-qCO53yBWTZYvM6N7BuRGPQfZWuhyH3Op8UN1OJ51LeDNo0ZQetw9JhfUKlmfWbXkImNHUl1ZmwJW0nJsKFtROH6kbnXh9QjQDoLYo7WaukkV6bRwz9W3hcOroHIk8-h1qEd1oTeOddpa_hm4tDoE0gaoVmOE4KIYAxOaQWjdPE4Q-CMIyF7MZSpRmtn3UQNQoAkHEbWzY3lYVNrx8ecBtQL3UdTBi1_zosbBigUmzdgaSXUfYBxMgkdrlci66Fq8U_D0fw4XMQ7vEn_-yJN-1RbxemJL-l8C9-5NjodYxa",
    "expires_in": 2680098475,
    "refresh_expires_in": 2680094875,
    "refresh_token": "eyJ0eXAiOiJhdCtqd3QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImIzZWU3NzY1MWFkNjM3ZTBmODllNDdkM2QwZmI3OGJhIn0.eyJpc3MiOiJodHRwczovL2lkcC5sb2NhbCIsImF1ZCI6ImFwaTEiLCJzdWIiOiI1YmU4NjM1OTA3M2M0MzRiYWQyZGEzOTMyMjIyZGFiZSIsImNsaWVudF9pZCI6Im15X2NsaWVudF9hcHAiLCJleHAiOjI2ODAwOTg0NzUsImlhdCI6MjY4MDA5NDg3NSwianRpIjoiNTZjYzg2NWI2YmFlYWZjNDBjZTI2NmY3YjIxMjA2YWYifQ.TAcFR4iqvhjZHBrNdfS6YK6CM7mMaQonVo4UpDfl3BFO_ZPrsO1DAE8NHBfVaEx19dJ0gmDqa3swj5eOe4w44vb_ecZACAw9gJCIyzsvVbtVa_Jcie6OE-FHpXPfPm26Nx5tbSu-_IlyB7-dvSiqN2-SNGHXmcAk8T55eyKXQE63MljgVU7iF9eGZpMDyKmN5SjvthuKxzRd1Tm_pm-nJ-qCO53yBWTZYvM6N7BuRGPQfZWuhyH3Op8UN1OJ51LeDNo0ZQetw9JhfUKlmfWbXkImNHUl1ZmwJW0nJsKFtROH6kbnXh9QjQDoLYo7WaukkV6bRwz9W3hcOroHIk8-h1qEd1oTeOddpa_hm4tDoE0gaoVmOE4KIYAxOaQWjdPE4Q-CMIyF7MZSpRmtn3UQNQoAkHEbWzY3lYVNrx8ecBtQL3UdTBi1_zosbBigUmzdgaSXUfYBxMgkdrlci66Fq8U_D0fw4XMQ7vEn_-yJN-1RbxemJL-l8C9-5NjodYxa",
    "token_type": "Bearer",
    "not-before-policy": 0,
    "session_state": "39b8f6d2-2ff6-4972-ad03-e100c8a1f40d",
    "scope": "email profile"
  }
};

server.use((req, res, next) => {
  console.log("POST request listener");
  const { body } = req;
  console.log(body);
  if (req.method === "POST" && req.path === "/identity-api/verify-address") {
    res.json(getVerifyJson());
  }
  else if (req.method === "POST" && req.path === "/core-api-v2/entity") {
    res.json();
  }
  else {
    next();
  }
});

server.use("/core-api-v2", router);

server.listen(3051, () => {
  console.log("JSON Server is running");
});
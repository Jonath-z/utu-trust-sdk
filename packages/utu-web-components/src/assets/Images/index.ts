import Logo from "./Logo";
import SadPenguin from "./SadPenguin";
import FlowerPenguin from "./FlowerPenguin";
import UttLogo from "./UttLogo";
import NoVideoLogo from"./video-slash-solid";
import NoBadgeLogo from "./certificate-solid";
import Star from "./star-solid";
import Stamp from "./stamp-solid";

export default Logo;

export { SadPenguin, FlowerPenguin, UttLogo, NoVideoLogo, NoBadgeLogo, Star, Stamp};



/* eslint-disable no-nested-ternary */
import { h } from "preact";
import {BaseComponent, getBaseProps} from "../../common/BaseComponent";
import { IFeedbackDetailsProps } from "./FeedbackDetailsProps";
import style from "./FeedbackDetails.scss";
import { EndorsementsList } from "../../endorsements/EndorsementsList";
import { StarRatingShow } from "../../starRating/StarRatingShow";
import BadgesShow from "../../badges/BadgesShow";
import { VideoShow } from "../../video/VideoShow";
import { ReviewShow } from "../../textReview/ReviewShow";
import {useFeedbackSummaryApi} from "../../../hooks/api";
import {ATTR_SOURCE_UUID,
  ATTR_TARGET_HUMAN_READABLE,
  ATTR_TARGET_TYPE,
  ATTR_TARGET_UUID,
  TAG_FEEDBACK_DETAILS_POPUP} from "../../../names";

export default function FeedbackDetails(props: IFeedbackDetailsProps) {
  const { apiUrl } = getBaseProps(props, TAG_FEEDBACK_DETAILS_POPUP);
  const targetType = props[ATTR_TARGET_TYPE];
  const targetHumanReadable = props[ATTR_TARGET_HUMAN_READABLE];
  const sourceUuid = props[ATTR_SOURCE_UUID];
  const targetUuid = props[ATTR_TARGET_UUID];

  const getTargetHumanReadableToDisplay = (): string => {
    let targetHumanReadableToReturn;
    switch (targetType) {
      case "address":
        targetHumanReadableToReturn = targetUuid;
        break;
      default:
        targetHumanReadableToReturn = targetHumanReadable;
    }
    return targetHumanReadableToReturn;
  }
  
  const targetHumanReadableToDisplay = getTargetHumanReadableToDisplay();
  const { feedbackSummary, submitStatus } = useFeedbackSummaryApi(apiUrl, sourceUuid, targetUuid);

  return (
    <BaseComponent style={style}>
      <div className="x-utu-feedback-details">
        <div className="x-utu-feedback-details_title">
          <p>Giving feedback for ( {targetHumanReadableToDisplay} )
            {targetType ? (
              <p>
              Signal Type: {targetType}
              </p>
            ) : ""}
          </p>
        </div>
        <div className="x-utu-feedback-details_body_1">
          <VideoShow {...props}
            feedbackSummary={feedbackSummary}
            submitStatus={submitStatus}
            setVideoVisibility={props.setVideoVisibility}/>
          <div className="x-utu-feedback-details_divider"/>
          <StarRatingShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        </div>
        <div className="x-utu-feedback-details_body_2">
          <BadgesShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
          <ReviewShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
          <EndorsementsList {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        </div>
      </div>
    </BaseComponent>
  );
}

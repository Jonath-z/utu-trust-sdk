import { h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent } from "../../common/BaseComponent";
import { IFeedbackProps } from "./FeedbackProps";

// styling
import style from "./FeedbackForm.scss";
// components
import { Badges } from "../../badges/Badges";
import { FeedbackTextInput } from "../../textReview/FeedbackTextInput";
import { EndorsementForm } from "../../endorsements/EndorsementForm";
// import { EndorsementsList } from "../EndorsementsList";
import { StarRatingInput } from "../../starRating/StarRatingInput";
// import RecordVideoContainer from "../RecordVideo/RecordVideoContainer";
import Logo from "../../../assets/Images"
import { ATTR_TARGET_HUMAN_READABLE, ATTR_TARGET_TYPE, ATTR_TARGET_UUID, ATTR_THEME_COLOR } from "../../../names";
import RecordVideo from "../../video/RecordVideo/RecordVideo";

export default function FeedbackForm(props: IFeedbackProps) {

  const targetType = props[ATTR_TARGET_TYPE];
  const targetUuid = props[ATTR_TARGET_UUID];
  const targetHumanReadable = props[ATTR_TARGET_HUMAN_READABLE];

  const getTargetHumanReadableToDisplay = (): string => {
    let targetHumanReadableToReturn;

    switch (targetType) {
      case 'address':
        targetHumanReadableToReturn = targetUuid;
        break;
      default:
        targetHumanReadableToReturn = targetHumanReadable;
    }

    return targetHumanReadableToReturn;
  }

  const targetHumanReadableToDisplay = getTargetHumanReadableToDisplay();

  // environments
  const [isDark, setIsDark] = useState(false);
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  /* eslint-disable react/jsx-props-no-spreading */
  return (
    <BaseComponent style={style} className="utu-feedback-form">

      <div className="mobile-view">
        {targetType ? (
          <div className="target-human-readable">
            Signal Type: {targetType} ( {targetHumanReadableToDisplay} )
          </div>
        ) : ''}

        <RecordVideo {...props} setVideoVisibility={props.setVideoVisibility} />
        <StarRatingInput {...props} />
        <Badges {...props} />
        <FeedbackTextInput {...props} />
        <EndorsementForm {...props} />
        <section className="logo-section-mobile">
          <div className="logo-position-mobile"><Logo /></div>
        </section>
      </div>

      <div className="desktop-view">
        <div className={`desktop-view-block-1 desktop-view-block-1-${isDark ? "dark" : "light"}`}>


        {targetType ? (
          <div className="target-human-readable">
            Signal Type: {targetType} ( {targetHumanReadableToDisplay} )
          </div>
        ) : ''}


          <div className="x-utu-feedback-form-video-stars">
            <RecordVideo {...props} setVideoVisibility={props.setVideoVisibility} />
            <div className="x-utu-divider" />
            <StarRatingInput {...props} />
          </div>

          <div className="x-utu-feedback-form-Badges">
            <Badges {...props} />
          </div>
        </div>
        {/* <div className="x-utu-feedback-form-Badges">
          <Badges {...props}/>
        </div> */}
        <div className={`desktop-view-block-2 desktop-view-block-2-${isDark ? "dark" : "light"
          }`
        }>
          <FeedbackTextInput {...props} />
        </div>
        <div className={`desktop-view-block-3 desktop-view-block-3-${isDark ? "dark" : "light"
          }`
        }>
          <EndorsementForm {...props} />
        </div>
        <section className="logo-section">
          <div className="logo-position-form"><Logo /></div>
        </section>
      </div>
    </BaseComponent>
  );
}



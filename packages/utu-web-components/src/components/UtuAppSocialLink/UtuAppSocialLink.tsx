import {h} from "preact";
import { useState } from "preact/hooks";
import { BaseComponent, getUtuSocialLink } from "../common/BaseComponent";
import style from "./UtuAppSocialLink.scss";
import {ATTR_BTN_COLOR, ATTR_THEME_COLOR} from "../../names";
import { IFeedbackDetailsProps } from "../screens/FeedbackDetails/FeedbackDetailsProps";

export default function UtuAppSocialLink(props: IFeedbackDetailsProps) {

  const {socialLinkUrl} = getUtuSocialLink();
  
  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition = isDark ? "dark" : "light"
  
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }
  
  return(
    <BaseComponent style={style} className={`x-utu-app-link x-utu-section x-utu-section-no-border-${envCondition}`}>
      <button 
        className="x-utu-app-link-btn x-utu-btn utu-btn-light" 
        type="button" 
        style={{backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}`}}
      >
        <a href={socialLinkUrl} className="x-utu-app-a" target="_blank" rel="noreferrer"> 
          <h3 className="x-utu-h3 x-utu-app-link-text">Connect to earn</h3>
        </a>
      </button>
    </BaseComponent>
  )
}